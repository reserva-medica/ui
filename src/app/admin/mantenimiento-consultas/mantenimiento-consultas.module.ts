import { NgModule } from '@angular/core';

import { MantenimientoConsultasRoutingModule } from './mantenimiento-consultas-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { ConsultaReservasComponent } from './pages/consulta-reservas/consulta-reservas.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    HomeComponent,
    ConsultaReservasComponent
  ],
  imports: [
    SharedModule,
    MantenimientoConsultasRoutingModule
  ]
})
export class MantenimientoConsultasModule { }
