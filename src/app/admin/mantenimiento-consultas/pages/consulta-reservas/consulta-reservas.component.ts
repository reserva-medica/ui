import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { HelpersService } from 'src/app/shared/services/helpers.service';
import { Estado, ReservaConsultaRequest, ReservaConsultaResponse } from '../../model/consulta.interface';
import { ConsultaService } from '../../services/consulta.service';

@Component({
  selector: 'app-consulta-reservas',
  templateUrl: './consulta-reservas.component.html',
  styleUrls: ['./consulta-reservas.component.scss']
})
export class ConsultaReservasComponent implements OnInit {

  primeraColumna:string[]=['datoBasico','datoSecundario','datoPago']
  columnas:string[]= ['cliente','tipoReserva','examen','email','dni','fechaReserva','horaInicio','horaFin','montoPago','formaPago','fechaReservaTemporal','estado']

  data = new MatTableDataSource<ReservaConsultaResponse>([])

  totalReserva=0

  comboPagina=[10,20,30]

  itemsPorPagina = 10
  miFormularioConsulta : FormGroup = this.fb.group({
    rango: this.fb.group({
      desde:[''],
      hasta:['']
    }),
    cEstado:['']
  })
  estados:Estado[]=[{valor:'I',descripcion:'Incompleto'},{valor:'C',descripcion:'Completo'}]
  @ViewChild(MatPaginator) paginacion!: MatPaginator
  constructor(
    private fb:FormBuilder,
    private consultaService:ConsultaService,
    private helpers: HelpersService
  ) { }

  ngOnInit(): void {
  }

  btnConsultar():void{
    console.log(this.miFormularioConsulta.value)
    
    const consultaRequest : ReservaConsultaRequest= {
      fechaInicio: this.helpers.fnParsearFechaFormatoSqlDate(this.miFormularioConsulta.value.rango?.desde || new Date(1900,0,1,0,0,0,0)),
      fechaFin: this.helpers.fnParsearFechaFormatoSqlDate(this.miFormularioConsulta.value.rango?.hasta || new Date(2900,0,1,0,0,0,0)),
      cEstado:this.miFormularioConsulta.value.cEstado
    }
    console.log(consultaRequest)
    this.consultaService.obtenerConsultaReserva(consultaRequest)
      .subscribe(res=>{
        console.log(res)
        this.data = new MatTableDataSource<ReservaConsultaResponse>(res.data)
        this.data.paginator = this.paginacion
        this.totalReserva = res.data.length
      })
  }
}
