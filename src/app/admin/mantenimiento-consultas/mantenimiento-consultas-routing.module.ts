import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultaReservasComponent } from './pages/consulta-reservas/consulta-reservas.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  {
    path:'',
    component: HomeComponent,
    children:[
      {
        path:'reservas',
        component: ConsultaReservasComponent
      },
      {
        path:'**',
        redirectTo:'reservas'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MantenimientoConsultasRoutingModule { }
