export interface ReservaConsultaRequest{
    fechaInicio: string
    fechaFin:string
    cEstado:string
}

export interface ReservaConsultaResponse{
    idReserva:number
    idTipoReserva:number
    idCliente:number
    idExamen:number
    fechaReservaTemporal:string
    fechaReservaTemporalDate?: Date
    fechaReserva:string
    fechaReservaDate?:Date
    horaInicio:string
    horaFin:string
    montoPago:number
    cEstado:string
    descripcionTipoReserva:string
    nombreCliente:string
    apellidoCliente:string
    emailCliente:string
    dniCliente:string
    descripcionFormaPago:string
    descripcionServicio:string
}

export interface Estado{
    valor:string
    descripcion:string
}