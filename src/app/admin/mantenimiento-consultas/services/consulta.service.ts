import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from 'src/app/shared/model/apiResponse.interface';
import { environment } from 'src/environments/environment';
import { ReservaConsultaRequest, ReservaConsultaResponse } from '../model/consulta.interface';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {


  baseUrl = environment.urlApi

  constructor(
    private http : HttpClient
  ) { }

  obtenerConsultaReserva(reserva:ReservaConsultaRequest):Observable<ApiResponse<ReservaConsultaResponse>>{
    return this.http.post<ApiResponse<ReservaConsultaResponse>>(`${this.baseUrl}/reserva/consultaReserva`,reserva)
  }
}
