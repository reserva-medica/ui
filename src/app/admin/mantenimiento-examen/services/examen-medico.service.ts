import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from 'src/app/shared/model/apiResponse.interface';
import { environment } from 'src/environments/environment';
import { Consultorio, ConsultorioRequest } from '../model/consultorio.interface';
import { Especialidad, EspecialidadRequest, Examen, Modalidad, Rol, TipoConsultorio, TipoConsultorioRequest } from '../model/examen.interface';
import { RequestExamen } from '../model/requestExamen.interface';
import { Usuario, UsuarioRequest } from '../model/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class ExamenMedicoService {

  private baseUrl = environment.urlApi

  constructor(
    private http : HttpClient
  ) { }

  listarExamenes():Observable<ApiResponse<Examen>>{
    return this.http.get<ApiResponse<Examen>>(`${this.baseUrl}/examen`)
  }
  registroExamen(examen:RequestExamen):Observable<ApiResponse<Examen>>{
    return this.http.post<ApiResponse<Examen>>(`${this.baseUrl}/examen`,examen)
  }

  obtenerRoles():Observable<ApiResponse<Rol>>{
    return this.http.get<ApiResponse<Rol>>(`${this.baseUrl}/usuario/roles`)
  }
  obtenerModalidades():Observable<ApiResponse<Modalidad>>{
    return this.http.get<ApiResponse<Modalidad>>(`${this.baseUrl}/usuario/modalidades`)
  }
  obtenerEspecialidades():Observable<ApiResponse<Especialidad>>{
    return this.http.get<ApiResponse<Especialidad>>(`${this.baseUrl}/usuario/especialidades`)
  }
  obtenerTipoConsultorios():Observable<ApiResponse<TipoConsultorio>>{
    return this.http.get<ApiResponse<TipoConsultorio>>(`${this.baseUrl}/consultorio/tipo`)
  }
  registrarEspecialidad(especialidad:EspecialidadRequest):Observable<ApiResponse<Especialidad>>{
    return this.http.post<ApiResponse<Especialidad>>(`${this.baseUrl}/usuario/especialidad`,especialidad)
  }
  registrarTipoConsultorio(tipoConsultorio:TipoConsultorioRequest):Observable<ApiResponse<TipoConsultorio>>{
    return this.http.post<ApiResponse<TipoConsultorio>>(`${this.baseUrl}/consultorio/tipo`,tipoConsultorio)
  }
  registrarUsuario(usuario:UsuarioRequest):Observable<ApiResponse<Usuario>>{
    return this.http.post<ApiResponse<Usuario>>(`${this.baseUrl}/usuario`,usuario)
  }
  registrarConsultorio(consultorio:ConsultorioRequest):Observable<ApiResponse<Consultorio>>{
    return this.http.post<ApiResponse<Consultorio>>(`${this.baseUrl}/consultorio`,consultorio)
  }
}
