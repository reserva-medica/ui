import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ConsultorioRequest } from '../../model/consultorio.interface';
import { Especialidad, EspecialidadRequest, Modalidad, Rol, TipoConsultorio, TipoConsultorioRequest } from '../../model/examen.interface';
import { RequestExamen } from '../../model/requestExamen.interface';
import { UsuarioRequest } from '../../model/usuario.interface';
import { ExamenMedicoService } from '../../services/examen-medico.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {

  miFormularioExamen:FormGroup = this.fb.group({
    descripcion: ['',Validators.required],
    monto:[0,Validators.min(0)]
  })
  miFormularioEspecialidad:FormGroup = this.fb.group({
    descripcion: ['',Validators.required],
    monto:[0,Validators.min(0)]
  })
  miFormularioTipoConsultorio:FormGroup = this.fb.group({
    descripcion: ['',Validators.required]
  })
  miFormularioUsuario:FormGroup= this.fb.group({
    nombre:['',Validators.required],
    apellido:['',Validators.required],
    descripcion:['',Validators.required],
    usuario:['',Validators.required],
    contrasena:['',Validators.required],
    email:['',Validators.required],
    rol:[0,Validators.min(1)],
    especialidad:[0],
    modalidad:[0]
  })
  miFormularioConsultorio:FormGroup = this.fb.group({
    nombre:['',Validators.required],
    monto:['',Validators.min(0)],
    tipoConsultorio:[0,Validators.min(1)]
  })
  roles:Rol[]=[]
  modalidades: Modalidad[]=[]
  especialidades: Especialidad[]=[]
  tipoConsultorios: TipoConsultorio[]=[]
  mostrarParaDoctor = false
  
  examen:RequestExamen={
    opcion:3,
    descripcion:'',
    monto:0
  }
  constructor(
    private fb: FormBuilder,
    private examenService: ExamenMedicoService,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.obtenerRoles()
    this.obtenerModalidades()
    this.obtenerEspecialidades()
    this.obtenerTipoConsultorios()
    this.miFormularioUsuario.controls['rol'].valueChanges
      .subscribe(valor=>{
          this.mostrarParaDoctor = valor === 3 ? true : false
      })
  }

  registrarExamen(): void{
    
    if(this.miFormularioExamen.invalid){
      return
    }
    this.examen = {opcion:3,...this.miFormularioExamen.value}
    console.log(this.examen)
    this.examenService.registroExamen(this.examen)
      .subscribe(res=>{
        this.snackBar.open('Registrado!!!','cerrar',{
          horizontalPosition: 'right',
          verticalPosition:'top',
          duration:5000
        })
        this.router.navigate(['/mantenimientoExamen'])
      })
  }

  registrarEspecialidad():void{

    if(this.miFormularioEspecialidad.invalid){
      return
    }
    const especialidad : EspecialidadRequest = {opcion:2,...this.miFormularioEspecialidad.value}
    console.log(especialidad)

   
    this.examenService.registrarEspecialidad(especialidad)
      .subscribe(res=>{
        console.log(res)
        this.miFormularioEspecialidad.reset({
          descripcion:'',
          monto:0
        })
        this.obtenerEspecialidades()
      })
    
  }

  registrarTipoConsultorio():void{
    if(this.miFormularioTipoConsultorio.invalid){
      return
    }
    const tipoConsultorio : TipoConsultorioRequest = {opcion:2,...this.miFormularioTipoConsultorio.value}
    console.log(tipoConsultorio)

    this.examenService.registrarTipoConsultorio(tipoConsultorio)
      .subscribe(res=>{
        console.log(res)
        this.miFormularioTipoConsultorio.reset({
          descripcion:''
        })
        this.obtenerTipoConsultorios()
      })
  }

  registrarUsuario():void{
    if(this.miFormularioUsuario.invalid){
      return
    }
    const usuario : UsuarioRequest = {opcion:2,...this.miFormularioUsuario.value}
    console.log(usuario)

    this.examenService.registrarUsuario(usuario)
      .subscribe(res=>{
        console.log(res)
        this.miFormularioUsuario.reset({
          rol:0,
          modalidad:0,
          especialidad:0
        })
        
      })
  }

  registrarConsultorio():void{
    console.log('entro')
    if(this.miFormularioConsultorio.invalid){
      return
    }
    const consultorio : ConsultorioRequest = {opcion:2,...this.miFormularioConsultorio.value}
   
    console.log(consultorio)
    this.examenService.registrarConsultorio(consultorio)
      .subscribe(res=>{
        console.log(res)
        this.miFormularioConsultorio.reset({
          tipoConsultorio:0
        })
        
      })
  }

  obtenerRoles():void{
    this.examenService.obtenerRoles()
      .subscribe(res=>{
        console.log(res)
        this.roles = res.data
      })
  }

  obtenerModalidades():void{
    this.examenService.obtenerModalidades()
      .subscribe(res=>{
        console.log(res)
        this.modalidades = res.data
      })
  }

  obtenerEspecialidades():void{
    this.examenService.obtenerEspecialidades()
      .subscribe(res=>{
        console.log(res)
        this.especialidades = res.data
      })
  }

  obtenerTipoConsultorios():void{
    this.examenService.obtenerTipoConsultorios()
      .subscribe(res=>{
        console.log(res)
        this.tipoConsultorios = res.data
      })
  }
}
