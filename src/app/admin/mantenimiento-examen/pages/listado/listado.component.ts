import { ViewChild,Component, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Examen } from '../../model/examen.interface';
import { ExamenMedicoService } from '../../services/examen-medico.service';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss']
})
export class ListadoComponent implements OnInit {


  columnas:string[]= ['descripcion','monto']

  data = new MatTableDataSource<Examen>()

  totalExamenes=0

  comboPagina=[5,10,20]

  itemsPorPagina = 5

  @ViewChild(MatPaginator) paginacion!: MatPaginator
 

  constructor(
    private examenService:ExamenMedicoService
  ) { }

  ngOnInit(): void {
    this.examenService.listarExamenes()
      .subscribe(res=>{
        console.log(res)
        this.data = new MatTableDataSource<Examen>(res.data)
        this.data.paginator = this.paginacion
        this.totalExamenes = res.data.length
      })
  }



}
