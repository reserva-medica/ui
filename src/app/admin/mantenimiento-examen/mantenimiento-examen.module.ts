import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';

import { MantenimientoExamenRoutingModule } from './mantenimiento-examen-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { ListadoComponent } from './pages/listado/listado.component';
import { RegistroComponent } from './pages/registro/registro.component';


@NgModule({
  declarations: [
    HomeComponent,
    ListadoComponent,
    RegistroComponent
  ],
  imports: [
    SharedModule,
    MantenimientoExamenRoutingModule
  ]
})
export class MantenimientoExamenModule { }
