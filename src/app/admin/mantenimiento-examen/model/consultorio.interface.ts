export interface Consultorio{
    idConsultorio:number
    nombreConsultorio:string
    monto:number
    idTipoConsultorio:number
    nombreTipoConsultorio:string
}

export interface ConsultorioRequest{
    opcion?:number
    idConsultorio?:number
    nombre:string
    monto:number
    idTipoConsultorio:number
}