export interface Examen{
  idExamen?: number
  descripcion: string
  monto:number
}

export interface Rol{
  idRol?:number
  titulo:string
  descripcion:string
}

export interface Especialidad{
  idEspecialidad?:number
  descripcionEspecialidad:string
}

export interface EspecialidadRequest{
  opcion?:number
  idEspecialidad?:number
  descripcion:string
  monto:number
}

export interface Modalidad{
  idModalidad?:number
  descripcionModalidad:string
}

export interface TipoConsultorio{
  idTipoConsultorio:number
  descripcion:string
}
export interface TipoConsultorioRequest{
  opcion?:number
  descripcion:string
}