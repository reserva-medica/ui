export interface UsuarioRequest{
    opcion?:number
    idUsuario?:number
    nombre:string
    apellido:string
    descripcion:string
    usuario:string
    contrasena:string
    email:string
    idRol:number
    idEspecialidad:number
    idModalidad:number
}

export interface Usuario{
    idUsuario:number
    nombreUsuario:string
    apellidoUsuario:string
    descripcionUsuario:string
    usuario:string
    contrasena:string
    email:string
}