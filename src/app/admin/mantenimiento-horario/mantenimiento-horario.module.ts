import { NgModule } from '@angular/core';

import { MantenimientoHorarioRoutingModule } from './mantenimiento-horario-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from './pages/home/home.component';
import { ListadoHorariosComponent } from './pages/listado-horarios/listado-horarios.component';
import { ModalActualizarTipoReservaComponent } from './components/modal-actualizar-tipo-reserva/modal-actualizar-tipo-reserva.component';


@NgModule({
  declarations: [
    HomeComponent,
    ListadoHorariosComponent,
    ModalActualizarTipoReservaComponent
  ],
  imports: [
    SharedModule,
    MantenimientoHorarioRoutingModule
  ]
})
export class MantenimientoHorarioModule { }
