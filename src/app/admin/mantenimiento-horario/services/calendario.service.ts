import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse } from 'src/app/shared/model/apiResponse.interface';
import { environment } from 'src/environments/environment';
import { CalendarioNoLaborable, CalendarioRequest, CalendarioResponse } from '../models/calendario.interface';

@Injectable({
  providedIn: 'root'
})
export class CalendarioService {


  private baseUrl = environment.urlApi

  constructor(
    private http:HttpClient
  ) { }

  obtenerFechasNoLaborables(fechaHoy:string):Observable<ApiResponse<CalendarioResponse>>{
    return this.http.get<ApiResponse<CalendarioResponse>>(`${this.baseUrl}/calendario?fecha=${fechaHoy}`)
  }

  registrarEliminarDiaNoLaborable(fecha:CalendarioRequest):Observable<ApiResponse<CalendarioNoLaborable>>{
    return this.http.post<ApiResponse<CalendarioNoLaborable>>(`${this.baseUrl}/calendario`,fecha)
  }
}
