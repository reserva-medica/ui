import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';

import { ApiResponse } from 'src/app/shared/model/apiResponse.interface';
import { environment } from 'src/environments/environment';
import { HorarioRegistroRequest, HorarioRequest } from '../models/horarioRequest.interface';
import { HorarioResponse } from '../models/horarioResponse.interface';
import { Listado } from '../models/listado.interface';

@Injectable({
  providedIn: 'root'
})
export class HorarioServiceService {


  private baseUrl = environment.urlApi

  constructor(
    private http : HttpClient
  ) { }

  obtenerListadoPorTipoReserva(idTipoReserva:number):Observable<ApiResponse<Listado>>{

    if(idTipoReserva===0){
      return of({
        status:true,
        data:[],
        mensaje:''
      })
    }

    return this.http.get<ApiResponse<Listado>>(`${this.baseUrl}/tipoReserva/${idTipoReserva}`)
  }
  // obtenerHorariosNoDisponiblePorTipoReserva(idTipoReserva:number,idEscogido:number):Observable<ApiResponse<HorarioResponse>>{
  //   return this.http.get<ApiResponse<HorarioResponse>>(`${this.baseUrl}/horario/${idTipoReserva}/${idEscogido}`)
  // }
  obtenerHorariosNoDisponiblePorTipoReserva(horarioRequest:HorarioRequest):Observable<ApiResponse<HorarioResponse>>{
    return this.http.post<ApiResponse<HorarioResponse>>(`${this.baseUrl}/horario/listarHorarioNoDisponible`,horarioRequest)
  }
  // eliminarHorariosPorTipoReserva(idTipoReserva:number,idEscogido:number):Observable<ApiResponse<HorarioResponse>>{
  //   return this.http.delete<ApiResponse<HorarioResponse>>(`${this.baseUrl}/horario/${idTipoReserva}/${idEscogido}`)
  // }
  eliminarHorariosPorTipoReserva(horario:HorarioRequest):Observable<ApiResponse<HorarioResponse>>{
      return this.http.post<ApiResponse<HorarioResponse>>(`${this.baseUrl}/horario/eliminarHorarioNoDisponible`,horario)
  }
  eliminarHorariosMasivoPorTipoReserva(horarios:HorarioRequest[]):Observable<ApiResponse<HorarioResponse>[]>{
    if(horarios.length===0){
      return of([])
    }
    const peticiones: Observable<ApiResponse<HorarioResponse>>[]= []

    horarios.forEach(horario=>{
      const peticion = this.eliminarHorariosPorTipoReserva(horario)
      peticiones.push(peticion)
    })

    return combineLatest(peticiones)
  }
  registrarHorariosPorTipoReserva(horario:HorarioRegistroRequest[]):Observable<ApiResponse<HorarioResponse>>{
    return this.http.post<ApiResponse<HorarioResponse>>(`${this.baseUrl}/horario`,horario)
  }

  // registrarHorarioMasivoPorTivoReserva(horarios:HorarioRequest[]):Observable<ApiResponse<HorarioResponse>[]>{
  //   if(horarios.length===0){
  //     return of([])
  //   }

  //   const peticiones: Observable<ApiResponse<HorarioResponse>>[]= []

  //   horarios.forEach(horario=>{
  //     const peticion = this.registrarHorariosPorTipoReserva(horario)
  //     peticiones.push(peticion)
  //   })

  //   return combineLatest(peticiones)
    
  // }
}
