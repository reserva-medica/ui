import { Component,ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  MatCalendarCellClassFunction, MatCalendarCellCssClasses, MatCalendarView } from '@angular/material/datepicker';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { switchMap, tap} from 'rxjs/operators'

import { TipoReserva } from 'src/app/shared/model/apiResponse.interface';
import { ApiServiceService } from 'src/app/shared/services/api-service.service';
import { HelpersService } from 'src/app/shared/services/helpers.service';
import { ModalActualizarTipoReservaComponent } from '../../components/modal-actualizar-tipo-reserva/modal-actualizar-tipo-reserva.component';
import { CalendarioNoLaborable, CalendarioRequest, CalendarioResponse } from '../../models/calendario.interface';
import { Horario } from '../../models/horario.interface';
import { HorarioRegistroRequest, HorarioRequest } from '../../models/horarioRequest.interface';
import { HorarioResponse } from '../../models/horarioResponse.interface';
import { Anio, Listado, Mes, Semana } from '../../models/listado.interface';
import { CalendarioService } from '../../services/calendario.service';
import { HorarioServiceService } from '../../services/horario-service.service';

@Component({
  selector: 'app-listado-horarios',
  templateUrl: './listado-horarios.component.html',
  styleUrls: ['./listado-horarios.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class ListadoHorariosComponent implements OnInit{

  


  dataTipoReserva = new MatTableDataSource<TipoReserva>([])
  columnaTipoReserva:string[] = ['descripcion','intervalo','cantidad','rangoInicio','rangoFinal','acciones']

  estadoAccion:boolean = true
 
  miFormulario:FormGroup = this.fb.group({
    tipoReserva:[0,Validators.required],
    listado:[0,Validators.required],
    anio:[0,Validators.required],
    mes:[0,Validators.required],
    semana:[0,Validators.required]
  })

  botonesCalendario = false
  botonHabilitado = true
  fechaMinima= new Date()
  fechaSeleccionada!: Date | null
  listadoFechaNoLaborable:CalendarioNoLaborable[]=[]
  vistaInicial: MatCalendarView | null = null
  //agregarClaseCalendario: MatCalendarCellClassFunction<Date> = (cellDate, view) => ''

  tipoReservas:TipoReserva[] = []
  listado: Listado[] = []
  anios: Anio[]=[]
  meses: Mes[]=[]
  semanas: Semana[]=[]
  mostrarMeses=false
  mostrarTabla= false
  mostrarSpinner=false
  fechaDel!:Date
  fechaHasta!:Date
  @ViewChild(MatPaginator) paginacion!: MatPaginator
  diasDisableados = [true,true,true,true,true,true,true]
  dias: string[]= ['domingo','lunes','martes','miercoles','jueves','viernes','sabado']
  columnas: string[]=['horario',...this.dias]
  data = new MatTableDataSource<Horario>([])
 

  constructor(
    private apiService:ApiServiceService,
    private horarioService:HorarioServiceService,
    private fb:FormBuilder,
    private helpers:HelpersService,
    private calendarioService: CalendarioService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.fnConstruirAnioMesSemana()
    this.obtenerListadoTipoReserva()
    this.obtenerFechas()
    this.miFormulario.controls['tipoReserva'].valueChanges
      .pipe(
        tap((_)=>{
          this.listado=[]
          this.miFormulario.controls['listado'].reset(0)
        }),
        switchMap(tipoReserva=> {
          
          const id = tipoReserva?.idTipoReserva || 0 
         
          return this.horarioService.obtenerListadoPorTipoReserva(id)
        })
      )
      .subscribe(res=>{
        this.listado = res.data
        
      })

    this.miFormulario.controls['listado'].valueChanges
      .subscribe(valor=>{
        // this.mostrarTabla=false
        this.miFormulario.controls['anio'].reset(0)
      })
    this.miFormulario.controls['anio'].valueChanges
      .subscribe(valor=>{
         
         this.miFormulario.controls['mes'].reset(0)
         this.mostrarMeses = valor === 0 ? false : true
      })
    this.miFormulario.controls['mes'].valueChanges
      .subscribe(valor=>{
        console.log(valor)
        this.miFormulario.controls['semana'].reset(0)

        this.semanas = []

        if(valor!==0){
          console.log('entro')
          const anio = this.miFormulario.controls['anio'].value
          const mes = valor
          const primerFecha = new Date(anio,mes-1,1,0,0,0,0)
          const ultimaFecha = new Date(anio,mes,1,0,0,0,0)
          const primerDia = new Date(anio,mes-1,1,0,0,0,0).getDay()
          let contSemanas = primerDia === 0 ? 0 : 1
          while(primerFecha<ultimaFecha){
            const dia = primerFecha.getDay()
            if(dia===0) contSemanas++

            primerFecha.setDate(primerFecha.getDate() + 1)

          }
          console.log({primerFecha,contSemanas})
          
          for(let i=1 ; i<=contSemanas ; i++){
            this.semanas.push({
              semana:i
            })
          }
        }

        
      })

    this.miFormulario.controls['semana'].valueChanges
      .subscribe(valor=>{
        this.mostrarTabla = false
        this.diasDisableados = [true,true,true,true,true,true,true]
        if(valor!==0){
          const anio = this.miFormulario.controls['anio'].value
          const mes = this.miFormulario.controls['mes'].value
          const semana = valor
          const primerFecha = new Date(anio,mes-1,1,0,0,0,0)
          const ultimaFecha = new Date(anio,mes,1,0,0,0,0)
         
          let contSemanas = 1
          let termina = false
          while(!termina){
            if(semana===contSemanas){
              termina = true
              this.fechaDel= new Date(primerFecha)
              while(semana===contSemanas){
                primerFecha.setDate(primerFecha.getDate()+1)
                const dia = primerFecha.getDay()
                if(dia===0){
                  contSemanas++
                }else{
                  this.fechaHasta = new Date(primerFecha)
                }
              }
            }else{
              primerFecha.setDate(primerFecha.getDate()+1)
              const dia = primerFecha.getDay()
              if(dia===0){
                  contSemanas++
              }
            }
          
          }
          if(this.fechaHasta>=ultimaFecha){
            ultimaFecha.setDate(ultimaFecha.getDate()-1)
            this.fechaHasta = ultimaFecha
          }

          const primerDia = this.fechaDel.getDay()
          const ultimoDia = this.fechaHasta.getDay()

          for(let i = primerDia ; i<=ultimoDia;i++){
            this.diasDisableados[i] = false
          }
          
         
          console.log(this.fechaDel,this.fechaHasta)
        }
      })
  }
 

  fnConstruirAnioMesSemana():void{
    this.anios = this.helpers.anios
    this.meses = this.helpers.meses


  }


 

  fnArmarIntervalos(rangoInicio:string,rangoFinal:string,intervalo:number,horariosNoDisponibles:HorarioResponse[]):void{
    if(intervalo === 0 ){
      this.data = new MatTableDataSource<Horario>([])

     
      return
    }
    //console.log(this.helpers.fnParsearHorarioaEntero(rangoInicio))
    const inicioRango = this.helpers.fnParsearHorarioaEntero(rangoInicio)
    const finRango= this.helpers.fnParsearHorarioaEntero(rangoFinal)

    const horaInicio = new Date(0,0,0,inicioRango[0],inicioRango[1],inicioRango[2],0)
    const horaFin = new Date(0,0,0,finRango[0],finRango[1],finRango[2],0)
    
    const difSegundos = (horaFin.getTime() - horaInicio.getTime())/1000 //segundos
    //console.log(difSegundos)
    const difMinutos = Math.floor(difSegundos/60) //minutos
    const cantidadIntervalos = Math.floor(difMinutos/intervalo)
    const horarios: Horario[] = []
    

    // const primerDia = this.fechaDel.getDay()
    // const ultimoDia = this.fechaHasta.getDay()
    for(let i = 0 ; i< cantidadIntervalos;i++){
      horarios.push({
        horario: new Date(0,0,0,inicioRango[0],inicioRango[1]+i*intervalo,inicioRango[2],0),
        dia:this.dias.map((dia,index)=>{

          let habilitado = true
          if(dia==='domingo'){
            habilitado = false
          }

          if(dia==='sabado'){
            const hora = new Date(0,0,0,inicioRango[0],inicioRango[1]+i*intervalo,inicioRango[2],0).getHours()
            if(hora>=14){
              habilitado=false
            }
          }
          // let deshabilitar = true

          // if(index>=primerDia && index<=ultimoDia){
          //   deshabilitar = false
          // }

          return{
            nombre: dia,
            habilitado
            // deshabilitar
          }
        })
      })
    }
    console.log(horarios)
    horarios.forEach(horario=>{
      const horarioNoDisponible = horariosNoDisponibles.filter(h=>{

        return h.horaInicio===this.helpers.fnObtenerHoraMinuto(horario.horario)
      })
      if(horarioNoDisponible.length>0){
        horario.dia.forEach(d=>{
           
           const nombreDia = horarioNoDisponible.filter(h=> h.dia === this.dias.indexOf(d.nombre))
           d.habilitado = nombreDia.length>0 ? false : true
        })
      }
    })
    this.data = new MatTableDataSource<Horario>(horarios)
    console.log(this.data)
  }

  setAll(completed:boolean,d:string):void{
   
    this.data.data.forEach(t=>{
       t.dia.forEach(s=>{
         if(s.nombre==d){
            s.habilitado = completed
         }
       })
    })
  }

  fnConsultarHorarioNoDisponibles():void{
    //console.log(this.miFormulario.value)
    //console.log(this.miFormulario.invalid)
    this.mostrarTabla = false
    this.mostrarSpinner=true
    const idTipoReserva = this.miFormulario.value.tipoReserva?.idTipoReserva || 0
    const intervalo = this.miFormulario.value.tipoReserva?.intervalo || 0
    const idEscogido = this.miFormulario.value.listado
    const anio = this.miFormulario.value.anio
    const mes = this.miFormulario.value.mes
    const semana = this.miFormulario.value.semana
    const rangoInicio = this.miFormulario.value.tipoReserva?.rangoInicio || '08:00:00'
    const rangoFinal = this.miFormulario.value.tipoReserva?.rangoFinal || '18:00:00'
    const horarioRequest:HorarioRequest = {
      opcion:2,
      idTipoReserva,
      idEscogido,
      anio,
      mes,
      semana
    }
    console.log(horarioRequest)
    this.horarioService.obtenerHorariosNoDisponiblePorTipoReserva(horarioRequest)
      .subscribe(res=>{
        console.log(res)
        // console.log(this.helpers.fnObtenerHoraMinuto(new Date(0,0,0,8,0,0,0)))
        this.fnArmarIntervalos(rangoInicio,rangoFinal,intervalo,res.data)
        this.mostrarTabla = true
        this.mostrarSpinner = false
        this.estadoAccion=true
      })
  }

  fnConfirmarModificacion():void{

    const idTipoReserva = this.miFormulario.value.tipoReserva?.idTipoReserva || 0
    const idEscogido = this.miFormulario.value.listado
    const anio = this.miFormulario.value.anio
    const mes = this.miFormulario.value.mes
    const semana = this.miFormulario.value.semana
    const intervalo = this.miFormulario.value.tipoReserva?.intervalo || 0


    const primerDia = this.fechaDel.getDay()
    const ultimoDia = this.fechaHasta.getDay()
    const horariosaEliminar : HorarioRequest[] = []
    for(let dia=primerDia; dia<=ultimoDia;dia++){
      const horarioEliminar:HorarioRequest = {
        idTipoReserva,idEscogido,anio,mes,semana,dia
      }
      horariosaEliminar.push(horarioEliminar)
    }


    this.mostrarTabla = false
    this.mostrarSpinner = true
    this.horarioService.eliminarHorariosMasivoPorTipoReserva(horariosaEliminar)
      .pipe(
        // switchMap((_)=>{
          
        // }),
        switchMap((_)=>{
          const horariosRegistro : HorarioRegistroRequest[] = []
          for(let dia=primerDia; dia<=ultimoDia;dia++){
            const horarioRegistro:HorarioRegistroRequest = {
              opcion:1,idTipoReserva,idEscogido,anio,mes,semana,dia,horarios:[]
            }
            
             // const horarios : HorarioRequest[] = []
              this.data.data.forEach(dato=>{
                
                dato.dia.forEach(d=>{
                  if(dia===this.dias.indexOf(d.nombre) && !d.habilitado){
                    horarioRegistro.horarios.push({
                      horaInicio:this.helpers.fnObtenerHoraMinuto(dato.horario),
                      horaFin:this.helpers.fnSumarIntervaloAfecha(dato.horario,intervalo)
                    })
                  }
                  // const dia = this.dias.indexOf(d.nombre)
                  // if(!d.habilitado && (dia>=primerDia && dia<=ultimoDia)){
                  //   const horario: HorarioRequest = {
                  //     opcion:1,
                  //     idTipoReserva:idTipoReserva,
                  //     idEscogido:idEscogido,
                  //     anio,
                  //     mes,
                  //     semana,
                  //     dia,
                  //     horaInicio:this.helpers.fnObtenerHoraMinuto(dato.horario),
                  //     horaFin:this.helpers.fnSumarIntervaloAfecha(dato.horario,intervalo)
          
                  //   }
                
                  //   horarios.push(horario)
                  // }
                })
              })
              if(horarioRegistro.horarios.length>0){
                horariosRegistro.push(horarioRegistro)
              }
              
          }
          console.log(horariosRegistro)
          return this.horarioService.registrarHorariosPorTipoReserva(horariosRegistro)
        })
      )
      .subscribe(res=>{
        this.fnConsultarHorarioNoDisponibles()
       
          
      })
  }

  fechaEscogida(fecha:Date | null){
    
   // console.log(fecha)
    this.fechaSeleccionada = fecha
    this.botonesCalendario = true
    const incluido = this.listadoFechaNoLaborable.filter(item=> item.fecha === this.helpers.fnParsearFechaFormatoSqlDate(fecha || new Date()))
    this.botonHabilitado = incluido.length>0

  }

  obtenerFechas():void{
    console.log(this.helpers.fnParsearFechaFormatoSqlDate(this.fechaMinima))
    this.calendarioService.obtenerFechasNoLaborables(this.helpers.fnParsearFechaFormatoSqlDate(this.fechaMinima))
    .subscribe(res=>{
      this.listadoFechaNoLaborable = res.data[0].calendarioNoLaborable
      console.log(this.listadoFechaNoLaborable)
      this.fechaSeleccionada = null
      this.vistaInicial = 'month'
      // this.agregarClaseCalendario = (cellDate,view)=>{
      //   //console.log(view)
      //   if(view==="month"){
      //      const incluido = this.listadoFechaNoLaborable.filter(item=> {

           
      //        return item.fecha === this.helpers.fnParsearFechaFormatoSqlDate(cellDate)
      //      })

      //      return incluido.length>0 ? 'fecha-nolaborable' : ''
      //   }

      //   return ''
      // }        
    })
  }


  obtenerListadoTipoReserva():void{

    this.apiService.listadoTipoReserva()
    .subscribe(res=>{
       
       this.tipoReservas = res.data
       this.dataTipoReserva = new MatTableDataSource<TipoReserva>(this.tipoReservas)
    })
   
  }

  btnHabilitaroDeshabilitarFecha():void{
    //console.log(this.botonHabilitado)
    const opcion = this.botonHabilitado ? 3:2
    //console.log(opcion)
    const calendarioRequest : CalendarioRequest = {
      opcion,
      fecha: this.helpers.fnParsearFechaFormatoSqlDate(this.fechaSeleccionada || new Date())
    }
    //console.log(calendarioRequest)
    this.botonesCalendario = false
    this.listadoFechaNoLaborable = []
    this.vistaInicial = null
    this.calendarioService.registrarEliminarDiaNoLaborable(calendarioRequest)
      .subscribe(res=>{
         console.log(res)
         this.obtenerFechas()
      })
  }

  btnCancelarCalendario():void{
    this.botonesCalendario = false
    this.fechaSeleccionada = null
  }

  agregarClaseCalendario():MatCalendarCellClassFunction<Date>{
    return (cellDate: Date): MatCalendarCellCssClasses=>{
      //console.log(view)
      const incluido = this.listadoFechaNoLaborable.filter(item=>item.fecha === this.helpers.fnParsearFechaFormatoSqlDate(cellDate))

      return incluido.length>0 ? 'fecha-nolaborable' : ''
         
     
    }        
  }

  btnActualizarTipoReserva(tipoReserva:TipoReserva):void{
    const tipo:TipoReserva = {...tipoReserva}
    
    const dialogo=this.dialog.open(ModalActualizarTipoReservaComponent,{
      minWidth:'40%',
      data: tipo
    })

    dialogo.afterClosed()
      .subscribe(res=>{
        console.log(res)
      })
  }
 

}
