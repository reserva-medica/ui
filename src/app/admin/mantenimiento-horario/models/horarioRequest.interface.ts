export interface HorarioRequest{
    opcion?:number
    idTipoReserva:number
    idEscogido:number
    anio:number
    mes:number
    semana:number
    dia?:number
    horaInicio?:string
    horaFin?:string
}

export interface HorarioRegistroRequest{
    opcion?:number
    idTipoReserva:number
    idEscogido:number
    anio:number
    mes:number
    semana:number
    dia:number
    horarios: HorasaRegistrar[]
}

export interface HorasaRegistrar{
    horaInicio:string
    horaFin:string
}