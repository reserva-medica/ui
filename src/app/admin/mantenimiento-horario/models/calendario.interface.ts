export interface CalendarioNoLaborable{
    idCalendarioNoLaborable: number
    fecha:string
    habilitado:string
}
export interface CalendarioResponse{
    calendarioNoLaborable: CalendarioNoLaborable[]
}
export interface CalendarioRequest{
    opcion:number
    fecha:string
}