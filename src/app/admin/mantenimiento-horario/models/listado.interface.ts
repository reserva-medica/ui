export interface Listado{
    id:number
    nombre:string
}

export interface Anio{
    anio:number
}

export interface Mes{
    mes:number
    valor:string
}

export interface Semana{
    semana:number
}