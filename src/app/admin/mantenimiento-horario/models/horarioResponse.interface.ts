export interface HorarioResponse{
    dia:number
    horaInicio:string
    horaFin:string
}