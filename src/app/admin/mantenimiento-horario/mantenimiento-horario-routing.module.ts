import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from 'src/app/cliente/pages/home/home.component';
import { ListadoHorariosComponent } from './pages/listado-horarios/listado-horarios.component';

const routes: Routes = [
  {
    path:'',
    component: HomeComponent,
    children: [
      {
        path:'horarios',
        component:ListadoHorariosComponent
      },
      {
        path:'**',
        redirectTo:'horarios'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MantenimientoHorarioRoutingModule { }
