import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TipoReserva } from 'src/app/shared/model/apiResponse.interface';

@Component({
  selector: 'app-modal-actualizar-tipo-reserva',
  templateUrl: './modal-actualizar-tipo-reserva.component.html',
  styles: [
  ]
})
export class ModalActualizarTipoReservaComponent implements OnInit {


  formularioTipoReserva:FormGroup = this.fb.group({
    descripcion:['',Validators.required],
    cantidad:['',Validators.required],
    intervalo:['',Validators.required]
  })

  constructor(
    private dialogRef: MatDialogRef<ModalActualizarTipoReservaComponent>,
    @Inject(MAT_DIALOG_DATA) public data : TipoReserva,
    private fb:FormBuilder
  ) { }

  ngOnInit(): void {
    console.log(this.data)
    this.formularioTipoReserva.reset({
      ...this.data
    })
    
  }

  btnConfirmar():void{
    console.log(this.formularioTipoReserva)
  }
  
  btnCerrar():void{
    this.dialogRef.close()
  }
}
