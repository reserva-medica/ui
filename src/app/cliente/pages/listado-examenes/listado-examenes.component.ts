import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Examen } from 'src/app/admin/mantenimiento-examen/model/examen.interface';
import { ExamenMedicoService } from 'src/app/admin/mantenimiento-examen/services/examen-medico.service';
import { ClienteService } from '../../services/cliente.service';

@Component({
  selector: 'app-listado-examenes',
  templateUrl: './listado-examenes.component.html',
  styleUrls: ['./listado-examenes.component.scss']
})
export class ListadoExamenesComponent implements OnInit {


  examenes:Examen[]= []

  constructor(
    private examenService: ExamenMedicoService,
    private router:Router,
    private clienteService: ClienteService
  ) { }

  ngOnInit(): void {

    this.examenService.listarExamenes()
      .subscribe(response=>{
        this.examenes = response.data
      })
  }

  btnReservar(idExamen:number):void{
    this.clienteService.reservaArealizar.idTipoReserva = 1
    this.clienteService.reservaArealizar.idEscogido = idExamen
    this.router.navigate(['/cliente/reservar'])
  }

}
