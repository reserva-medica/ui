import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { HelpersService } from 'src/app/shared/services/helpers.service';
import { FormaPago, PagosRequest, PagosResponse } from '../../models/pago.interface';
import { ClienteService } from '../../services/cliente.service';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.scss']
})
export class PagosComponent implements OnInit {


  pagos: PagosRequest ={
    fecha: '',
    reservas : []
  }
  selectFormaPago:FormControl = this.fb.control(0,Validators.min(1))
  // pagosAmostrar:PagosResponse[] = []
  formaPagos: FormaPago[]=[]
  columnas:string[]= ['descripcion','tipoReserva','fechaReserva','horaInicio','horaFin','monto']

  data = new MatTableDataSource<PagosResponse>()
  constructor(
    private clienteService: ClienteService,
    private helper : HelpersService,
    private fb:FormBuilder,
    private snackBar: MatSnackBar,
    private router:Router
  ) { }

  ngOnInit(): void {
    if(this.clienteService.pagosArealizar.length===0){
      this.router.navigate(['/'])
      return
    }
    console.log(this.clienteService.pagosArealizar)
    console.log(this.helper.fnParseaStringDate("2021-07-05"))
    this.pagos.reservas = this.clienteService.pagosArealizar
    this.pagos.fecha = this.helper.fnParsearFechaFormatoSqlDate(new Date())
    this.obtenerPagos()
    this.obtenerFormaPago()
  }

  obtenerPagos():void{
    this.clienteService.obtenerPagos(this.pagos)
      .subscribe(res=>{
        //  this.pagosAmostrar = res.data
         console.log(res.data)
         res.data.forEach(pago=>{
            if(pago.descripcionExamen.length>0){
              pago.descripcion = pago.descripcionExamen
            }
            if(pago.descripcionUsuario.length>0){
              pago.descripcion = pago.descripcionUsuario
            }
            if(pago.descripcionConsultorio.length>0){
              pago.descripcion = pago.descripcionConsultorio
            }
           pago.fechaDate = this.helper.fnParseaStringDate(pago.fechaReserva)
           pago.fechaInicioDate =this.helper.fnParseaStringDate(pago.fechaReserva,pago.horaInicio)
           pago.fechaFinDate = this.helper.fnParseaStringDate(pago.fechaReserva,pago.horaFin)
         })
         this.data = new MatTableDataSource<PagosResponse>(res.data)
      })
  }

  obtenerFormaPago():void{
    this.clienteService.obtenerFormaPagos()
      .subscribe(res=>{
        console.log(res)
        this.formaPagos = res.data
      })
  }

  obtenerTotal():number{

    let total = 0

    this.data.data.forEach(d=>{
       total+= d.montoPago
    })
    return total
  }


  btnPagarReservas():void{
    this.pagos.fecha = this.helper.fnParsearFechaFormatSqlDatetime(new Date())
    this.pagos.idFormaPago = this.selectFormaPago.value
    this.clienteService.realizarPago(this.pagos)
      .subscribe(res=>{
        this.snackBar.open('Pago realizado!!!','cerrar',{
          horizontalPosition: 'right',
          verticalPosition:'top',
          duration:5000
        })
        this.clienteService.pagosArealizar = []
        this.router.navigate(['/cliente'])
      })
  }
}
