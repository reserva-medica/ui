
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { TipoReserva } from 'src/app/shared/model/apiResponse.interface';
import { HelpersService } from 'src/app/shared/services/helpers.service';
import { ValidacionesService } from 'src/app/shared/validators/validaciones.service';
import { ModalEscogerDeNuevoComponent } from '../../components/modal-escoger-de-nuevo/modal-escoger-de-nuevo.component';
import { ModalRegistrarDeNuevoComponent } from '../../components/modal-registrar-de-nuevo/modal-registrar-de-nuevo.component';
import { ReservaCompletaRequest, ReservaParcial, ReservaParcialRequest } from '../../models/cliente.interface';
import {  FechaParaCliente, FechaRequest, FechaResponse, HorarioNoDisponible } from '../../models/fechaNoDisponible.interface';
import { ClienteService } from '../../services/cliente.service';

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.scss']
})
export class ReservaComponent implements OnInit {


  @ViewChild('stepper') stepper!:MatStepper

  idTipoReserva:number = 0
  datosTipoReserva!: TipoReserva 
  idEscogido:number = 0
  idReserva:ReservaParcial[] = []
  esEditable = true
  miFormulario:FormGroup = this.fb.group({
    realizoReserva:[false],
    nombre:['',[Validators.required]],
    apellido:['',[Validators.required]],
    email:['',[Validators.required,Validators.pattern(this.validacionesService.emailPattern)]],
    telefono:['',[Validators.required]],
    dni:['']
  })
  miFormularioFecha:FormGroup = this.fb.group({
    fechaEscogido:['',Validators.required],
    horarioEscogido:['',Validators.required]
  })
  // miFormularioHorario:FormGroup = this.fb.group({
  //   horarioEscogido:['',Validators.required]
  // })
  get telefonoField():FormControl{
    return this.miFormulario.get('telefono') as FormControl
  }
  miFormularioSecundario:FormGroup = this.fb.group({
    dni:['',Validators.required],
    distrito:[''],
    edad:[''],
    descuento:[''],
    tipoComprobante:[false],
    ruc:[''],
    razonSocial:['']
  })

  fechaEscogida!:Date | null 
  realizoReservaAntes = false
  estadoFormularioInicio = false
  tipoComprobante = false
  fechaMinima= new Date()

  fechasAValidar: FechaResponse[]= []
  horarioNoDisponible: HorarioNoDisponible[]=[]
  horariosParaCliente: FechaParaCliente[]=[]
  validarDias= (d:Date):boolean=>{
    return true
  } 
  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private helper: HelpersService,
    private clienteService: ClienteService,
    private snackBar: MatSnackBar,
    private router:Router,
    private dialog: MatDialog,
    private validacionesService: ValidacionesService
  ) { }

  ngOnInit(): void {
    // this.idTipoReserva = this.clienteService.reservaArealizar.idTipoReserva
    // this.idEscogido = this.clienteService.reservaArealizar.idEscogido
    // console.log(this.helper.fnParsearFechaFormatoSqlDate(this.fechaMinima))
    // this.clienteService.obtenerDatosDelTipoReserva(this.idTipoReserva)
    //   .subscribe(res=>{
    //       this.datosTipoReserva = res.data[0]
    //       console.log(this.datosTipoReserva)
    //   })
    this.fnAccionNuevaReserva()
    this.miFormulario.controls['realizoReserva'].valueChanges
      .subscribe(valor=>{
        this.estadoFormularioInicio = true
        this.realizoReservaAntes = valor
      })
      
 

    this.miFormularioSecundario.controls['tipoComprobante'].valueChanges
      .subscribe(valor=>{
        this.tipoComprobante = valor
      })
  }
 
  fnAccionNuevaReserva():void{
    if(this.clienteService.reservaArealizar.idEscogido===0){
      this.router.navigate(['/'])
      return
    }
    this.horariosParaCliente = []
    this.fechaEscogida = null
    this.miFormularioFecha.reset({
      fechaEscogido:'',
      horarioEscogido:''
    })
    console.log(this.clienteService.reservaArealizar)
    
    this.idTipoReserva = this.clienteService.reservaArealizar.idTipoReserva
    this.idEscogido = this.clienteService.reservaArealizar.idEscogido
    //console.log(this.helper.fnParsearFechaFormatoSqlDate(this.fechaMinima))
    this.clienteService.obtenerDatosDelTipoReserva(this.idTipoReserva)
      .subscribe(res=>{
          this.datosTipoReserva = res.data[0]
          console.log(this.datosTipoReserva)
      })

      const fechaRequest:FechaRequest = {
        fecha: this.helper.fnParsearFechaFormatoSqlDate(this.fechaMinima),
        idTipoReserva:this.idTipoReserva,
        idEscogido: this.idEscogido
      }
      console.log(fechaRequest)
      this.clienteService.obtenerFechasNoDisponibles(fechaRequest)
        .subscribe(res=>{
           this.fechasAValidar = res.data
           console.log(res.data)
           this.validarDias = (d:Date):boolean=>{
                const validar = this.fechasAValidar[0].fechaNoDisponible.filter(f=>f.fecha===this.helper.fnParsearFechaFormatoSqlDate(d))
                let habilitar = d.getDay() == 0 ? false : true
                validar.forEach(f=>{
                    if(f.habilitado==='1'){
                      habilitar = false
                    }else{
                      habilitar = true
                    }
                })
                // const dia = this.fechasAValidar[0].diaNoDisponible.filter(day=>day.dia===d.getDay())
  
                
                //return dia!==0
                // return validar.length===0 && dia.length===0
                return habilitar
            }
        })
  }

  btnRealizarReservaParcial():void{
    console.log(this.miFormulario.value)
    if(this.miFormulario.invalid){
      this.miFormulario.markAllAsTouched()
      return
    }
    

    const reserva:ReservaParcialRequest = {
      opcion:1,
      nombre:this.miFormulario.value.nombre,
      apellido:this.miFormulario.value.apellido,
      email:this.miFormulario.value.email,
      telefono:this.miFormulario.value.telefono,
      idTipoReserva:this.idTipoReserva,
      idEscogido:this.idEscogido,
      fechaTemporal: this.helper.fnParsearFechaFormatSqlDatetime(new Date())
    }

    console.log(reserva)

    this.clienteService.registrarReservaParcial(reserva)
      .subscribe(res=>{
        console.log(res)
        this.idReserva = res.data
      }) 

  }
  btnRealizarReservaClienteValidado():void{
    const reserva:ReservaParcialRequest = {
      opcion:1,
      nombre:'',
      apellido:'',
      email:'',
      telefono:'',
      idReserva:this.idReserva[0].idReserva,
      idTipoReserva:this.idTipoReserva,
      idEscogido:this.idEscogido,
      fechaTemporal: this.helper.fnParsearFechaFormatSqlDatetime(new Date())
    }
    console.log("----cliente validado------")
    console.log(reserva)

    this.clienteService.registrarNuevaReserva(reserva)
      .subscribe(res=>{
        console.log(res)
        this.idReserva = res.data
      }) 
  }
  btnRealizarReservaCompleta():void{
    console.log(this.miFormularioSecundario.value)
    if(this.miFormularioSecundario.invalid){

      this.miFormularioSecundario.markAllAsTouched()
      return
    }
  
    
    console.log( this.helper.fnParsearFechaFormatoSqlDate(this.miFormularioFecha.value.fechaEscogido))
    console.log(this.helper.fnObtenerHoraMinuto(this.miFormularioFecha.value.horarioEscogido))

    const intervalo = this.datosTipoReserva?.intervalo || 0

    const reserva:ReservaCompletaRequest= {
      opcion:2,
      idReserva:this.idReserva[0].idReserva,
      idEscogido:this.idEscogido,
      idDescuento:0,
      idTipoReserva:this.idTipoReserva,
      fechaReserva : this.helper.fnParsearFechaFormatoSqlDate(this.fechaEscogida || new Date()),
      horaInicio: this.helper.fnObtenerHoraMinuto(this.miFormularioFecha.value.horarioEscogido),
      horaFin: this.helper.fnSumarIntervaloAfecha(this.miFormularioFecha.value.horarioEscogido,intervalo),
      dni: this.miFormularioSecundario.value.dni,
      distrito: this.miFormularioSecundario.value.distrito,
      edad:this.miFormularioSecundario.value.edad,
      ruc: this.miFormularioSecundario.value.tipoComprobante ? this.miFormularioSecundario.value.ruc : '',
      razonSocial: this.miFormularioSecundario.value.tipoComprobante ?  this.miFormularioSecundario.value.razonSocial : '',
      fechaTemporal: this.helper.fnParsearFechaFormatSqlDatetime(new Date())
    }

    console.log(reserva)

    this.clienteService.registrarReserva(reserva)
      .subscribe(res=>{
        console.log(res)
        this.clienteService.pagosArealizar.push(this.idReserva[0].idReserva)
        const dialogo = this.dialog.open(ModalRegistrarDeNuevoComponent,{
           disableClose: true
        })
        dialogo.afterClosed().subscribe(res=>{
          console.log(res)
          if(res){
            const dialogoResetear= this.dialog.open(ModalEscogerDeNuevoComponent,{
              disableClose:true
            })
            dialogoResetear.afterClosed().subscribe(res=>{
              this.stepper.selectedIndex = 1
              this.fnAccionNuevaReserva()
              this.btnRealizarReservaClienteValidado()
            })

          }else{
            this.router.navigate(['/cliente/pagos'])
          }
        })
        // this.router.navigate(['/cliente/pagos'])
      })

  }

  btnValidarDNI():void{
    const dni = this.miFormulario.controls['dni'].value
    console.log(dni)
  }
  fnArmarHorariosSegunFecha(e:Date | null):void{
    const intervalo = this.datosTipoReserva?.intervalo || 0

    if(intervalo === 0 ) return

    const rangoInicio = this.datosTipoReserva?.rangoInicio || '08:00:00'
    const rangoFinal = this.datosTipoReserva?.rangoFinal || '18:00:00'

    const inicioRango = this.helper.fnParsearHorarioaEntero(rangoInicio)
    const finRango= this.helper.fnParsearHorarioaEntero(rangoFinal)

    const horaInicio = new Date(0,0,0,inicioRango[0],inicioRango[1],inicioRango[2],0)
    const horaFin = new Date(0,0,0,finRango[0],finRango[1],finRango[2],0)
    
    const difSegundos = (horaFin.getTime() - horaInicio.getTime())/1000 //segundos
    //console.log(difSegundos)
    const difMinutos = Math.floor(difSegundos/60) //minutos
    const cantidadIntervalos = Math.floor(difMinutos/intervalo)
    
    this.horariosParaCliente = []
    
    for(let i = 0 ; i< cantidadIntervalos;i++){
      const fecha = new Date(0,0,0,inicioRango[0],inicioRango[1]+i*intervalo,inicioRango[2],0)
      const dia = (e || new Date())
      
      const deshabilitado = dia.getDay() !==6 ? false : (fecha.getHours()>=14 ? true : false)
      
      this.horariosParaCliente.push({
        fecha,
        deshabilitado
      })
      const existe =  this.horarioNoDisponible.filter(f=>f.horaInicio === this.helper.fnObtenerHoraMinuto(fecha))
      
      if(existe.length>0){
        this.horariosParaCliente[i].deshabilitado = true
      }
   
      
    }
  
    
    if(this.horariosParaCliente.length===0){
      this.snackBar.open('Lo siento se agotaron los horarios!!','cerrar',{
        horizontalPosition: 'right',
        verticalPosition:'top',
        duration:5000
      })
  
      return
    }

    this.miFormularioFecha.reset({
      fechaEscogido:e
    })
  }
  escogerFecha(e:Date | null):void{
    
    
    this.fechaEscogida = e
    console.log(this.fechaEscogida)
    const fechaRequest:FechaRequest = {
      fecha: this.helper.fnParsearFechaFormatoSqlDate(this.fechaEscogida || new Date()),
      idTipoReserva:this.idTipoReserva,
      idEscogido: this.idEscogido
    }
    console.log(fechaRequest)
    this.clienteService.obtenerHorariosNoDisponiblesSegunFecha(fechaRequest)
      .subscribe(res=>{
        console.log(res.data)
        this.horarioNoDisponible = res.data
        this.fnArmarHorariosSegunFecha(e)
        
      })
  }

  campoEsValidoFormBasico(campo:string):boolean{
    return this.miFormulario.controls[campo].touched &&
           this.miFormulario.controls[campo].invalid
  }

  campoEsValidoFormSecundario(campo:string):boolean{
    return this.miFormularioSecundario.controls[campo].touched &&
           this.miFormularioSecundario.controls[campo].invalid
  }

  

}
