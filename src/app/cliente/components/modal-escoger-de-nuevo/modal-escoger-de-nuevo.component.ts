import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { switchMap, tap } from 'rxjs/operators';
import { Listado } from 'src/app/admin/mantenimiento-horario/models/listado.interface';
import { HorarioServiceService } from 'src/app/admin/mantenimiento-horario/services/horario-service.service';
import { TipoReserva } from 'src/app/shared/model/apiResponse.interface';
import { ApiServiceService } from 'src/app/shared/services/api-service.service';
import { ClienteService } from '../../services/cliente.service';

@Component({
  selector: 'app-modal-escoger-de-nuevo',
  templateUrl: './modal-escoger-de-nuevo.component.html',
  styleUrls: ['./modal-escoger-de-nuevo.component.scss']
})
export class ModalEscogerDeNuevoComponent implements OnInit {


  miFormularioTipoReserva: FormGroup = this.fb.group({
    tipoReserva:[0,Validators.min(1)],
    listado:[0,Validators.min(1)]
  })

  tipoReservas: TipoReserva[]=[]
  listado: Listado[] = []
  constructor(
    private apiService: ApiServiceService,
    private horarioService:HorarioServiceService,
    private fb:FormBuilder,
    private dialogRef: MatDialogRef<ModalEscogerDeNuevoComponent>,
    private clienteService:ClienteService
  ) { }

  ngOnInit(): void {
    this.apiService.listadoTipoReserva()
    .subscribe(res=>{
       
       this.tipoReservas = res.data
       
    })
    this.miFormularioTipoReserva.controls['tipoReserva'].valueChanges
      .pipe(
        tap((_)=>{
          this.listado=[]
          this.miFormularioTipoReserva.controls['listado'].reset(0)
        }),
        switchMap(tipoReserva=> {
          
          const id = tipoReserva
         
          return this.horarioService.obtenerListadoPorTipoReserva(id)
        })
      )
      .subscribe(res=>{
        this.listado = res.data
        
      })
  }

  confirmar():void{
    console.log(this.miFormularioTipoReserva.value)
    if(this.miFormularioTipoReserva.invalid){
      return
    }

    this.clienteService.reservaArealizar.idTipoReserva = this.miFormularioTipoReserva.controls['tipoReserva'].value 
    this.clienteService.reservaArealizar.idEscogido = this.miFormularioTipoReserva.controls['listado'].value 
    this.dialogRef.close()
  }

}
