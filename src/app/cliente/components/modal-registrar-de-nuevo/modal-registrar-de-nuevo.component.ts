import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-modal-registrar-de-nuevo',
  templateUrl: './modal-registrar-de-nuevo.component.html',
  styleUrls: ['./modal-registrar-de-nuevo.component.scss']
})
export class ModalRegistrarDeNuevoComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<ModalRegistrarDeNuevoComponent>
  ) { }

  ngOnInit(): void {
  }

  nuevaReserva():void{
    this.dialogRef.close(true)
  }
  pagar():void{
    this.dialogRef.close(false)
  }
}
