import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClienteRoutingModule } from './cliente-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { ListadoExamenesComponent } from './pages/listado-examenes/listado-examenes.component';
import { SharedModule } from '../shared/shared.module';
import { ReservaComponent } from './pages/reserva/reserva.component';
import { PagosComponent } from './pages/pagos/pagos.component';
import { ModalRegistrarDeNuevoComponent } from './components/modal-registrar-de-nuevo/modal-registrar-de-nuevo.component';
import { ModalEscogerDeNuevoComponent } from './components/modal-escoger-de-nuevo/modal-escoger-de-nuevo.component';


@NgModule({
  declarations: [
    HomeComponent,
    ListadoExamenesComponent,
    ReservaComponent,
    PagosComponent,
    ModalRegistrarDeNuevoComponent,
    ModalEscogerDeNuevoComponent
  ],
  imports: [
    ClienteRoutingModule,
    SharedModule
  ]
})
export class ClienteModule { }
