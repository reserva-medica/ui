import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ListadoExamenesComponent } from './pages/listado-examenes/listado-examenes.component';
import { PagosComponent } from './pages/pagos/pagos.component';
import { ReservaComponent } from './pages/reserva/reserva.component';

const routes: Routes = [
  {
    path:'',
    component:HomeComponent,
    children:[
      {
        path:'listadoExamen',
        component:ListadoExamenesComponent
      },
      {
        path:'reservar',
        component:ReservaComponent
      },
      {
        path:'pagos',
        component:PagosComponent
      },
      {
        path:'**',
        redirectTo:'listadoExamen'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClienteRoutingModule { }
