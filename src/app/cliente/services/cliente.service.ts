import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponse, TipoReserva } from 'src/app/shared/model/apiResponse.interface';
import { environment } from 'src/environments/environment';
import { ReservaCompletaRequest, ReservaParcial, ReservaParcialRequest } from '../models/cliente.interface';
import { FechaNoDisponible, FechaRequest, FechaResponse, HorarioNoDisponible } from '../models/fechaNoDisponible.interface';
import { FormaPago, PagosRequest, PagosResponse, ReservaRealizar } from '../models/pago.interface';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  reservaArealizar: ReservaRealizar = {
    idEscogido:0,
    idTipoReserva:0
  }
  pagosArealizar:number[]= []
  baseUrl= environment.urlApi

  constructor(
    private http:HttpClient
  ) { }

  registrarReservaParcial(reserva:ReservaParcialRequest):Observable<ApiResponse<ReservaParcial>>{
    return this.http.post<ApiResponse<ReservaParcial>>(`${this.baseUrl}/reserva/cliente`,reserva)
  }

  registrarReserva(reserva:ReservaCompletaRequest):Observable<ApiResponse<ReservaParcial>>{
    return this.http.post<ApiResponse<ReservaParcial>>(`${this.baseUrl}/reserva`,reserva)
  }
  //ESTA RESERVA ES CON UN CLIENTE YA VALIDADO
  registrarNuevaReserva(reserva:ReservaParcialRequest):Observable<ApiResponse<ReservaParcial>>{
    return this.http.post<ApiResponse<ReservaParcial>>(`${this.baseUrl}/reserva/clienteValidado`,reserva)
    
  }

  obtenerFechasNoDisponibles(fecha:FechaRequest):Observable<ApiResponse<FechaResponse>>{
    return this.http.post<ApiResponse<FechaResponse>>(`${this.baseUrl}/reserva/fechasNoDisponiblesExamen`,fecha)
  }

  obtenerHorariosNoDisponiblesSegunFecha(fecha:FechaRequest):Observable<ApiResponse<HorarioNoDisponible>>{
    return this.http.post<ApiResponse<HorarioNoDisponible>>(`${this.baseUrl}/reserva/HorarioNoDisponiblesExamenSegunFecha`,fecha)
  }
  obtenerDatosDelTipoReserva(idTipoReserva:number):Observable<ApiResponse<TipoReserva>>{
    return this.http.get<ApiResponse<TipoReserva>>(`${this.baseUrl}/tipoReserva/escogido/${idTipoReserva}`)
  }

  obtenerPagos(pagos:PagosRequest):Observable<ApiResponse<PagosResponse>>{
    return this.http.post<ApiResponse<PagosResponse>>(`${this.baseUrl}/reserva/pagos`,pagos)
  }

  obtenerFormaPagos():Observable<ApiResponse<FormaPago>>{
    return this.http.get<ApiResponse<FormaPago>>(`${this.baseUrl}/formaPago`)
  }

  realizarPago(pagos:PagosRequest):Observable<ApiResponse<PagosResponse>>{
    return this.http.post<ApiResponse<PagosResponse>>(`${this.baseUrl}/reserva/realizarPago`,pagos)
  }
}
