export interface FechaNoDisponible{
    fecha:string
    habilitado:string
}

export interface DiaNoDisponible{
    dia:number
}

export interface FechaResponse{
    fechaNoDisponible:FechaNoDisponible[]
    diaNoDisponible: DiaNoDisponible[]
}

export interface FechaRequest{
    fecha:string
    idTipoReserva:number
    idEscogido:number
}

export interface HorarioNoDisponible{
    horaInicio:string
    horaFin:string
}

export interface FechaParaCliente{
    fecha:Date
    deshabilitado:boolean
}