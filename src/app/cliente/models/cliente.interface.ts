export interface ReservaParcial{
    idReserva: number
}
export interface ReservaParcialRequest{
    opcion?:number
    nombre:string
    apellido:string
    email:string
    telefono:string
    idReserva?:number
    idTipoReserva:number
    idEscogido:number
    fechaTemporal:string
}

export interface ReservaCompletaRequest{
    opcion?:number
    idReserva:number
    idTipoReserva:number
    fechaReserva:string
    horaInicio:string
    horaFin:string
    idDescuento:number
    idEscogido:number
    dni:string
    distrito?:string
    edad?:number
    ruc?:string
    razonSocial?:string
    fechaTemporal:string
}