export interface PagosRequest{
    fecha:string
    reservas: number[]
    idFormaPago?:number
}

export interface PagosResponse{
    descripcion?:string
    descripcionExamen: string
    descripcionUsuario:string
    descripcionConsultorio:string
    descripcionTipoReserva:string
    montoPago:number
    fechaReserva:string
    horaInicio:string
    horaFin:string
    fechaDate?: Date
    fechaInicioDate?:Date
    fechaFinDate?:Date
}

export interface FormaPago{
    idFormaPago:number
    descripcion:string
    habilitado:string
}

export interface ReservaRealizar{
    idTipoReserva:number
    idEscogido:number
}