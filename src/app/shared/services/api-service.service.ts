import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiResponse, TipoReserva } from '../model/apiResponse.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {


  private baseUrl = environment.urlApi

  constructor(
    private http: HttpClient
  ) { }

  listadoTipoReserva(): Observable<ApiResponse<TipoReserva>>{
    return this.http.get<ApiResponse<TipoReserva>>(`${this.baseUrl}/tipoReserva`)
  }
}
