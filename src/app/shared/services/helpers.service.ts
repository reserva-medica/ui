import { Injectable } from '@angular/core';
import { Anio, Mes } from 'src/app/admin/mantenimiento-horario/models/listado.interface';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {


  anios: Anio[]= []
  meses: Mes[] = []

  constructor() { 
    const anioActual = new Date()
    this.anios = [
      {
        anio: anioActual.getFullYear()
      },
      {
        anio: anioActual.getFullYear() +1 
      }
    ]

    this.meses = [
      {
        mes:1,
        valor:'enero'
      },
      {
        mes:2,
        valor:'febrero'
      },
      {
        mes:3,
        valor:'marzo'
      },
      {
        mes:4,
        valor:'abril'
      },
      {
        mes:5,
        valor:'mayo'
      },
      {
        mes:6,
        valor:'junio'
      },
      {
        mes:7,
        valor:'julio'
      },
      {
        mes:8,
        valor:'agosto'
      },
      {
        mes:9,
        valor:'setiembre'
      },
      {
        mes:10,
        valor:'octubre'
      },
      {
        mes:11,
        valor:'noviembre'
      },
      {
        mes:12,
        valor:'diciembre'
      }
    ]

  }

  fnObtenerHoraMinuto(fecha:Date):string{

    const hora = this.fnParsearAstring(fecha.getHours())
    const minuto = this.fnParsearAstring(fecha.getMinutes())
    const segundo = this.fnParsearAstring(fecha.getSeconds())
    //console.log(`${hora}:${minuto}:${segundo}`)
    return `${hora}:${minuto}:${segundo}`
  }

  fnParsearAstring(valor:number):string{
    
    if(valor<10) return `0${valor}`

    return `${valor}`
  }

  fnSumarIntervaloAfecha(fecha:Date,intervalo:number):string{

    const fechaFinal = new Date(fecha)
    fechaFinal.setMinutes(fechaFinal.getMinutes()+intervalo)

    return this.fnObtenerHoraMinuto(fechaFinal)
  }

  fnParsearFechaFormatoSqlDate(fecha:Date):string{


    const anio = this.fnParsearAstring(fecha.getFullYear())
    const mes =this.fnParsearAstring(fecha.getMonth()+1)
    const dia = this.fnParsearAstring(fecha.getDate())
    //console.log({anio,mes,dia})

    return `${anio}-${mes}-${dia}`
  }

  fnParsearHorarioaEntero(horario:string):number[]{
      const items = horario.split(':').map(h=> parseInt(h))
      return items

  }

  fnParsearFechaFormatSqlDatetime(fecha:Date):string{
    const parteDate = this.fnParsearFechaFormatoSqlDate(fecha)
    const parteTime = this.fnObtenerHoraMinuto(fecha)

    return `${parteDate} ${parteTime}`
  }

  fnParseaStringDate(fecha:string,hora="00:00:00"):Date{

    const valor = new Date(`${fecha}T${hora}`)

    return valor
  }

  
}
