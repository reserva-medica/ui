export interface ApiResponse<T>{
    status:boolean
    data: T[]    
    mensaje: string
}

export interface TipoReserva{
    idTipoReserva:number
    descripcion:string
    intervalo:number
    cantidad:number
    rangoInicio:string
    rangoFinal:string
}