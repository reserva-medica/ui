import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SoloNumerosDirective } from './directives/solo-numeros.directive';



@NgModule({
  declarations: [
    SoloNumerosDirective
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  exports:[
    CommonModule,
    SoloNumerosDirective,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class SharedModule { }
