import {  Directive, ElementRef} from '@angular/core';
import {  NgControl } from '@angular/forms';


@Directive({
  selector: '[soloNumeros]',
  host:{
    '(input)': 'validarNumero($event)'
  }
})
export class SoloNumerosDirective {

  numeroPattern:RegExp = new RegExp(/[^0-9]*/g)
 
  constructor(
    private control:NgControl,
    private el: ElementRef<HTMLInputElement>
  ) { 
   
  }

  

  validarNumero(e:KeyboardEvent):void{
    console.log('entra')

    const valorInicial = this.el.nativeElement.value
    this.control.control?.setValue(valorInicial.replace(this.numeroPattern, ''))
   
      

  }

  
}
