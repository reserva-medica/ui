import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:'cliente',
    loadChildren: ()=> import('./cliente/cliente.module').then(m=>m.ClienteModule)
  },
  {
    path:'mantenimientoServicios',
    loadChildren: ()=> import('./admin/mantenimiento-examen/mantenimiento-examen.module').then(m=>m.MantenimientoExamenModule)
  },
  {
    path:'mantenimientoHorario',
    loadChildren: ()=> import('./admin/mantenimiento-horario/mantenimiento-horario.module').then(m=>m.MantenimientoHorarioModule)
  },
  {
    path:'mantenimientoConsultas',
    loadChildren: ()=> import('./admin/mantenimiento-consultas/mantenimiento-consultas.module').then(m=>m.MantenimientoConsultasModule)
  },
  {
    path:'**',
    redirectTo:'cliente'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
